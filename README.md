# elastic-at-a-glance.py

## What it Does

Gives a quick overview of Wikimedia's Elastic clusters. As of this writing,
we're focused on [upgrading to bullseye](https://phabricator.wikimedia.org/T289135) ,
so the script provides only OS-related information, sourced from the *_nodes*
API route within the Elastic API.

## Invoking

Without any arguments, the script will connect to localhost on the default
cleartext port (9200):
```
./elastic-at-a-glance.py
Cluster production-search-codfw has 43 bullseye nodes and 3 stretch nodes
Node elastic2059.codfw.wmnet is running Debian GNU/Linux 9 (stretch)
Node elastic2028.codfw.wmnet is running Debian GNU/Linux 9 (stretch)
Node elastic2060.codfw.wmnet is running Debian GNU/Linux 9 (stretch)
```
It's also possible to specify cluster ID and/or port:
### Cluster ID only ###
```
./elastic-at-a-glance.py cloudelastic
Cluster cloudelastic-chi-eqiad has 6 bullseye nodes and 0 stretch nodes
```
### Cluster ID and port ###
```
./elastic-at-a-glance.py codfw -p 9400
Cluster production-search-omega-codfw has 21 bullseye nodes and 2 stretch nodes
Node elastic2059.codfw.wmnet is running Debian GNU/Linux 9 (stretch)
Node elastic2028.codfw.wmnet is running Debian GNU/Linux 9 (stretch)
```

## Known Issues ##
- The script does not work from anywhere. You should be on a bastion, maintenance
server, or elastic host. Not all elastic hosts listen on all ports; you may have
to try a different host for the smaller clusters (`psi` and `omega`).
- The script requires the Python Elasticsearch libraries and
[plac](https://plac.readthedocs.io/en/latest/)
for command-line parsing. Within the WMF, both are available as Debian packages.
