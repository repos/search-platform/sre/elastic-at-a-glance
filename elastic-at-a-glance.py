#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0

# from elasticsearch import ConflictError, Elasticsearch, RequestError, TransportError
from prettytable import PrettyTable

from typing import DefaultDict, Dict, Iterable, Iterator, List, Optional, Sequence

import elasticsearch
import inspect
import json
import logging
import plac
import requests
import sys


# elastic-at-a-glance.py,
# displays


def construct_vip_url(cluster_id, cluster_port):

# code lifted directly from spicerack ref:
# https://github.com/wikimedia/operations-software-spicerack/blob/637ab7b536c9cb8528afadcdcab964c2b24fed21/spicerack/elasticsearch_cluster.py#L23
    ELASTICSEARCH_CLUSTERS: Dict[str, Dict[str, Dict[str, str]]] = {
        "search": {
            "search_eqiad": {
                "production-search-eqiad": "https://search.svc.eqiad.wmnet:9243",
                "production-search-omega-eqiad": "https://search.svc.eqiad.wmnet:9443",
                "production-search-psi-eqiad": "https://search.svc.eqiad.wmnet:9643",
            },
            "search_codfw": {
                "production-search-codfw": "https://search.svc.codfw.wmnet:9243",
                "production-search-omega-codfw": "https://search.svc.codfw.wmnet:9443",
                "production-search-psi-codfw": "https://search.svc.codfw.wmnet:9643",
            },
            "relforge": {
                "relforge-eqiad": "https://relforge1002.eqiad.wmnet:9243",
                "relforge-eqiad-small-alpha": "https://relforge1002.eqiad.wmnet:9443",
            },
            "cloudelastic": {
                "cloudelastic-chi-https": "https://cloudelastic.wikimedia.org:9243",
                "cloudelastic-omega-https": "https://cloudelastic.wikimedia.org:9443",
                "cloudelastic-psi-https": "https://cloudelastic.wikimedia.org:9643",
            },
        }
    }


    # "Magic" to specify cluster URL with minimal input from user
    # select on first 2 port numbers (92, 94, etc) which are the same for
    # cleartext or encrypted
    cluster_port_prefix = cluster_port[0:2]
    for cluster in ELASTICSEARCH_CLUSTERS["search"].keys():
        if cluster_id in cluster:
            cluster_urls = list(ELASTICSEARCH_CLUSTERS["search"][cluster].values())
            for url in cluster_urls:
                if cluster_port_prefix in url:
                    cluster_url = url
            break
        else:
            cluster_url = "http://127.0.0.1:{}".format(cluster_port)
    return cluster_url

def get_node_info(es_client):
    node_info = es_client.nodes.info()
    # creating a custom node object with more cluster-level info
    custom_node_info = {**node_info, **{'cluster_os': {'bullseye': 0}}}
    custom_node_info["cluster_os"]["stretch"] = 0
    for node_key in node_info["nodes"].keys():
        if "bullseye" in (node_info["nodes"][node_key]["os"]["pretty_name"]):
            custom_node_info["cluster_os"]["bullseye"] += 1
        elif "stretch" in (node_info["nodes"][node_key]["os"]["pretty_name"]):
            custom_node_info["cluster_os"]["stretch"] += 1
    return custom_node_info

@plac.annotations(
    cluster_id=plac.Annotation("Cluster ID", "positional", None, str,
                               choices=["codfw", "eqiad", "cloudelastic", "relforge", "localhost"]),
    cluster_port=plac.Annotation("Cluster cleartext port", "option", "p", choices=[
                                  "9200", "9400", "9600"])
)
def main(cluster_id="localhost", cluster_port="9200"):
    vip_url = construct_vip_url(cluster_id, cluster_port)
    es_client = elasticsearch.Elasticsearch(vip_url)
    try:
        node_info = es_client.nodes.info()
    except elasticsearch.exceptions.ConnectionError:
            print (f"Error! Couldn't connect to {vip_url} . If invoking script from \
{cluster_id}, try a different host as this one is probably not listening on {cluster_port}")
            sys.exit(1)
    custom_node_info = get_node_info(es_client)
    print("Cluster {} has {} bullseye nodes and {} stretch nodes".format(custom_node_info["cluster_name"],
                                                                         custom_node_info["cluster_os"]["bullseye"],
                                                                         custom_node_info["cluster_os"]["stretch"]))

    for node_key in custom_node_info["nodes"].keys():
        if "stretch" in (custom_node_info["nodes"][node_key]["os"]["pretty_name"]):

            print ("Node {} is running {}".format(custom_node_info["nodes"][node_key]["attributes"]["fqdn"],
                                                 custom_node_info["nodes"][node_key]["os"]["pretty_name"]))



if __name__ == '__main__':
    plac.call(main)
